from django.contrib import admin
from .models import Item, Categorie, Etat

# Register your models here.
admin.site.register(Item)
admin.site.register(Categorie)
admin.site.register(Etat)