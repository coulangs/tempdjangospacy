from django.shortcuts import render
from django.http import JsonResponse
from .models import Item, Categorie
import json 

# print("Import de spacy...")
# import spacy 

# modele = "fr_core_news_md"
# print("Import de du modèle", modele, "...")
# nlp = spacy.load(modele)


def home(request):
    return render(request, 'home.html')



def variables(request):
    nom = "Marie"
    hobbies = ["ping pong", "lecture", "musique"]
    return render(request, 'variables.html', { "utilisateur":nom, "activités":hobbies })



def spacy(request):
    doc = nlp("Ceci est une phrase d'exemple.")

    tokens = []

    for token in doc:
        tokens.append([token.text, token.lemma_, token.pos_])

    return render(request, 'spacy.html', {"tokens":tokens})


def analyze(request):
    colis = json.loads(request.body)
    text = colis['inText']
    print('Réception :', text)

    output = nlp(text)

    rep = []
    for token in output:
        rep.append([token.text, token.pos_])

    reponse = {
        "reponse": rep
    }

    return JsonResponse(reponse)


def todolist(request):
    categories = Categorie.objects.all()
    return render(request, 'todolist.html', {"categories":categories})


def listItems(request):
    items = Item.objects.all()

    # items = Item.objects.filter(categorie="perso") # Récupérer seulement les items dont la categorie est "perso"
    # item = Item.objects.first() # Récupérer le dernier item ajouté (le premier de la liste)
    # item = Item.objects.filter(categorie="famille").first() # idem parmi les items de la catégorie "famille"

    list_items = []

    for item in items:
        if item.categorie!=None:
            cat = item.categorie.nom
        else:
            cat = ""
        list_items.append( {"titre":item.titre, "texte":item.texte, "id":item.id, "categorie":cat} )

    reponse = {
        "items": list_items
    }

    return JsonResponse(reponse)


def addItem(request):
    colis = json.loads(request.body)
    titre = colis["titre"]
    texte = colis["texte"]
    categorie = colis["categorie"]
    newItem = Item(titre=titre,texte=texte,categorie=Categorie.objects.get(id=categorie))
    newItem.save()

    reponse = {
        "resultat": 1
    }

    return JsonResponse(reponse)


def delItem(request):
    colis = json.loads(request.body)
    identifiant = colis["identifiant"]

    item = Item.objects.get(id=identifiant)
    item.delete()

    reponse = {
        "resultat": 1
    }

    return JsonResponse(reponse)