print("Import de spacy...")
import spacy 

modele = "fr_core_news_md"
print("Import de du modèle", modele, "...")
nlp = spacy.load(modele)

doc = nlp("Ceci est une phrase d'exemple.")
print(doc.text)

for token in doc:
    print(token.text, token.lemma_, token.pos_, token.morph.to_dict(), token.dep_, token.shape_)